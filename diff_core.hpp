//
// Created by igor on 15.01.18.
//

#ifndef PDIFF_DIFF_CORE_H
#define PDIFF_DIFF_CORE_H

#include <string>
#include <vector>


typedef std::vector<std::string> string_vector;
typedef unsigned long ulong;

/**
 * Container and algo for longest common sequence calculation
 */
class lcs_matrix {
public:
    lcs_matrix(const string_vector &pA, const string_vector &pB);

    ~lcs_matrix();

    string_vector calculate() const;

private:
    ulong **mArray;
    const string_vector &mA;
    const string_vector &mB;
};

// Helper - specifies diff item
struct diff_item {
    char modifier;  // '+' - line to be added, '-' - removed, ' ' - untouched line
    ulong source_line; // line number in source file
    ulong dest_line; // line number in modified file

    diff_item(const char &pModifier, const ulong &pSourceLine, const ulong &pDestLine) :
            modifier(pModifier), source_line(pSourceLine), dest_line(pDestLine) {}
};


// Helper - specifies borders of diff hunk
struct diff_hunk {
    ulong start;
    ulong end;

    diff_hunk(const ulong &pStart, const ulong &pEnd) : start(pStart), end(pEnd) {}
};


class diff_core {
public:
    diff_core(const string_vector &pA, const string_vector &pB);

    /**
     * Calculates hunks' start and end positions and their amount
     * @return Amount of diff hunks
     */
    ulong hunks();

    /**
     * Produces lines if i-th diff hunk
     * @param i - hunk id
     * @return hunk diff lines
     */
    string_vector hunk(const ulong &i) const;

    /**
     * Produces lines for all diff hunks
     * @return diff lines
     */
    string_vector diff() const;

private:
    void calculate_diff();

    /**
     * Determine hunk start position counting context lines
     * @param pHunk
     * @return starting line number
     */
    ulong hunk_start(const diff_hunk &pHunk) const;

    /**
     * Determine hunk end position counting context lines
     * @param pHunk
     * @return end line number
     */
    ulong hunk_end(const diff_hunk &pHunk) const;

    const string_vector &mA;
    const string_vector &mB;
    string_vector mCommon;
    std::vector<diff_item> mDiff;
    std::vector<diff_hunk> mHunks;
};

#endif //PDIFF_DIFF_CORE_H
