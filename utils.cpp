//
// Created by igor on 19.01.18.
//

#include "utils.hpp"

#include <algorithm>
#include <ctime>
#include <fstream>
#include <iostream>
#include <iterator>
#include <sys/stat.h>


bool file_exists(const std::string &pFileName) {
    struct stat buffer;
    return (stat(pFileName.c_str(), &buffer) == 0);
}

std::string file_mtime(const std::string &pFileName) {
    return std::string("");
    //disabled because of compatibility issues between linux headers and macos env
//    struct stat buffer;
//    if (stat(pFileName.c_str(), &buffer)) {
//        return "";
//    }
//    struct tm const *tm = localtime(&buffer.st_mtim.tv_sec);
//    const size_t bufLen = 40;
//    char stringBuffer[bufLen] = {0};
//    strftime(stringBuffer, bufLen, "%F %T", tm);
//    std::string time = stringBuffer;
//    snprintf(stringBuffer, bufLen, ".%09lu", buffer.st_mtim.tv_nsec);
//    time += stringBuffer;
//    strftime(stringBuffer, bufLen, " %z", tm);
//    time += stringBuffer;
//    return time;
}

std::vector<std::string> read_file(const std::string &pFileName) {
    if (!file_exists(pFileName)) {
        throw std::invalid_argument("File " + pFileName + " does not exist");
    }
    std::vector<std::string> lines;
    std::string line;
    std::ifstream file(pFileName);
    while (!file.eof()) {
        std::getline(file, line);
        lines.emplace_back(line);
    }
    return lines;
}
