//
// Created by igor on 15.01.18.
//

#include "diff_core.hpp"

#include <algorithm>
#include <iostream>

#define SAFE_DELETE_ARRAY(A) if (A) { delete[] (A); (A)=nullptr; };


lcs_matrix::lcs_matrix(const string_vector &pA, const string_vector &pB) : mA(pA), mB(pB) {
    ulong rows(mA.size() + 1); // + 1 because we need one zeroed row and one zeroed column
    ulong cols(mB.size() + 1);
    mArray = new ulong *[rows];
    for (ulong i = 0; i < rows; ++i) {
        mArray[i] = new ulong[cols];
    }
}

lcs_matrix::~lcs_matrix() {
    if (mArray) {
        for (ulong i = 0; i < mA.size() + 1; ++i) {
            SAFE_DELETE_ARRAY(mArray[i]);
        }
        SAFE_DELETE_ARRAY(mArray);
    }
}

/**
 * Calculates longest common sequence of given two mA and mB
 * @return std::vector<std::string>
 */
string_vector lcs_matrix::calculate() const {
    // classic version of LCS algo without use of additional "directions" matrix
    for (ulong i = 0; i <= mA.size(); ++i) {
        for (ulong j = 0; j <= mB.size(); ++j) {
            if (!i || !j) {
                mArray[i][j] = 0;
            } else if (mA[i - 1] == mB[j - 1]) {
                mArray[i][j] = mArray[i - 1][j - 1] + 1;
            } else {
                mArray[i][j] = std::max(mArray[i - 1][j], mArray[i][j - 1]);
            }
        }
    }

    string_vector lcs;
    ulong i = mA.size();
    ulong j = mB.size();
    while (i && j) {
        if (mA[i - 1] == mB[j - 1]) {
            lcs.push_back(mA[i - 1]);
            --i;
            --j;
        } else if (mArray[i - 1][j] > mArray[i][j - 1]) {
            --i;
        } else {
            --j;
        }
    }
    std::reverse(lcs.begin(), lcs.end());
    return lcs;
}

diff_core::diff_core(const string_vector &pA, const string_vector &pB) : mA(pA), mB(pB) {
    lcs_matrix matrix(mA, mB);
    mCommon = matrix.calculate();
    calculate_diff();
    hunks();
}

void diff_core::calculate_diff() {
    const ulong aSize(mA.size());
    const ulong bSize(mB.size());
    const ulong cSize(mCommon.size());

    if (aSize == bSize && aSize == cSize) {
        return;
    }
    ulong aIdx(0);
    ulong bIdx(0);
    ulong cIdx(0);

    // let's iterate over common sequence at first
    for (cIdx = 0; cIdx < cSize; ++cIdx) {
        while (mA[aIdx] != mCommon[cIdx]) {
            mDiff.emplace_back('-', aIdx, 0);
            ++aIdx;
        }
        while (mB[bIdx] != mCommon[cIdx]) {
            mDiff.emplace_back('+', 0, bIdx);
            ++bIdx;
        }
        mDiff.emplace_back(' ', aIdx, bIdx);
        ++aIdx;
        ++bIdx;
    }
    for (; aIdx < aSize; ++aIdx) {
        mDiff.emplace_back('-', aIdx, 0);
    }
    for (; bIdx < bSize; ++bIdx) {
        mDiff.emplace_back('+', 0, bIdx);
    }
}

ulong diff_core::hunks() {
    if (mDiff.empty() || !mHunks.empty()) {
        return mHunks.size();
    }
    std::vector<diff_hunk> spaces;
    // "6" is because universal diff output format uses 3 lines before and after diff segment as context.
    // Two diff hunks should be joined if their context lines overlap.
    const ulong MIN_UNTOUCHED = 6;
    ulong sequentialUntouched(0);
    ulong begin;
    ulong i(0);

    for (; i < mDiff.size(); ++i) {
        if (' ' == mDiff[i].modifier) {
            if (!sequentialUntouched) {
                begin = i;
            }
            ++sequentialUntouched;
        } else {
            if (sequentialUntouched > MIN_UNTOUCHED) {
                spaces.emplace_back(begin, i - 1);
            }
            sequentialUntouched = 0;
        }
    }
    if (sequentialUntouched > MIN_UNTOUCHED) {
        spaces.emplace_back(begin, i - 1);
    }

    // now we convert spaces (between modified parts) to diff hunks
    if (spaces.empty()) {
        mHunks.emplace_back(0, mDiff.size() - 1);
        return 1;
    }
    const ulong diffSize(mDiff.size());
    for (i = 0; i < spaces.size(); ++i) {
        if (spaces.size() - 1 == i) {
            if (spaces[i].end != diffSize - 1) {
                mHunks.emplace_back(spaces[i].end + 1, diffSize - 1);
            }
            break;
        }
        if (!i) {
            if (spaces[i].start) {
                mHunks.emplace_back(0, spaces[i].start - 1);
            }
        }
        mHunks.emplace_back(spaces[i].end + 1, spaces[i + 1].start - 1);
    }
    return mHunks.size();
}

string_vector diff_core::hunk(const ulong &i) const {
    string_vector lines;
    const diff_hunk &diffHunk(mHunks[i]);

    ulong sourceStart(0);
    ulong destStart(0);
    ulong sourceSum(0);
    ulong destSum(0);

    for (ulong diffIdx = hunk_start(diffHunk); diffIdx < hunk_end(diffHunk) + 1; ++diffIdx) {
        const diff_item &item(mDiff[diffIdx]);
        if (!sourceStart && (' ' == item.modifier || '-' == item.modifier)) {
            sourceStart = item.source_line + 1;
        }
        if (!destStart && (' ' == item.modifier || '+' == item.modifier)) {
            destStart = item.dest_line + 1;
        }
        switch (item.modifier) {
            case '-':
                ++sourceSum;
                lines.push_back("-" + mA[item.source_line]);
                break;
            case '+':
                ++destSum;
                lines.push_back("+" + mB[item.dest_line]);
                break;
            default:
                ++sourceSum;
                ++destSum;
                lines.push_back(" " + mA[item.source_line]);
                break;
        }
    }
    std::string head(
            "@@ -" + std::to_string(sourceStart)
            + "," + std::to_string(sourceSum)
            + " +" + std::to_string(destStart)
            + "," + std::to_string(destSum)
            + " @@");
    lines.insert(lines.begin(), head);
    return lines;
}

ulong diff_core::hunk_start(const diff_hunk &pHunk) const {
    ulong start(pHunk.start);
    ulong count(3);
    // since we use ulong we cannot just substract 3, because we can't have negative numbers
    while (count && start > 0) {
        --start;
        --count;
    }
    return start;
}

ulong diff_core::hunk_end(const diff_hunk &pHunk) const {
    ulong last(mDiff.size() - 1);
    ulong end = pHunk.end + 3;
    return end > last ? last : end;
}

string_vector diff_core::diff() const {
    string_vector lines;
    for (ulong i = 0; i < mHunks.size(); ++i) {
        string_vector hunk_lines = hunk(i);
        lines.insert(lines.end(), hunk_lines.begin(), hunk_lines.end());
    }
    return lines;
}
