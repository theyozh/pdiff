
#include <iostream>

#include "diff_core.hpp"
#include "utils.hpp"

using namespace std;

void print_usage(const bool &pWrongArgumentsAmount) {
    if (pWrongArgumentsAmount) {
        // cout is intentionally used insted of cerr for usage simplification
        std::cout << "Wrong amount of arguments were passed." << std::endl;
    }

    std::cout << R"help(
In order to show this message pass -h flag:
    pdiff -h

In order to get diff of two files pass their names as arguments. Example:
    pdiff file1 file2

In order to save diff of two files into patch file do the following:
    pdiff file1 file2 > file.patch

    , where file1, file2, and file.patch are examples of file names.

In case of identical files no output will be produced.
If two files are different then patch in unified-like format will be printed.

In order to patch some file pass its name and name of patch file preceded by -p flag:
    pdiff file1 -p file.patch
)help";
}

/**
 * Diff calculation entry point and output headers generation
 * @param pArgs
 */
void diff(const string_vector &pArgs) {
    string_vector a = read_file(pArgs[0]); // original
    string_vector b = read_file(pArgs[1]); // modified
    diff_core diff(a, b);
    if (diff.hunks()) {
        std::cout << "--- " << pArgs[0] << "\t" << file_mtime(pArgs[0]) << std::endl;
        std::cout << "+++ " << pArgs[1] << "\t" << file_mtime(pArgs[1]) << std::endl;
        string_vector diffLines = diff.diff();
        for (auto &line: diffLines) {
            std::cout << line << std::endl;
        }
    }
}

int main(int argc, char *argv[]) {
    string_vector args;
    args.assign(argv + 1, argv + argc); // arguments array conversion into handy format
    int retCode(0);

    switch (args.size()) {
        case 1:
            if ("-h" == args[0]) {
                print_usage(false);
            } else {
                print_usage(true);
                retCode = 1;
            }
            break;
        case 2:
            for (auto &file: args) {
                if (!file_exists(file)) {
                    std::cout << "File \"" << file << "\" does not exist. Exiting" << std::endl;
                    return 2;
                }
            }
            diff(args);
            break;
        case 3:
            if ("-p" != args[1]) {
                print_usage(true);
                retCode = 1;
            } else {
                if (!file_exists(args[0])) {
                    std::cout << "File \"" << args[0] << "\" does not exist. Exiting" << std::endl;
                    return 2;
                }
                if (!file_exists(args[2])) {
                    std::cout << "File \"" << args[2] << "\" does not exist. Exiting" << std::endl;
                    return 2;
                }
                std::cout << "Not implemented" << std::endl;
                retCode = 7;
            }
            break;
        default:
            print_usage(true);
            retCode = 1;
    }

    return retCode;
}