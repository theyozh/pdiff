//
// Created by igor on 21.01.18.
//

#ifndef PDIFF_PATCH_CORE_H
#define PDIFF_PATCH_CORE_H

#include "diff_core.hpp"


class patch_core {
public:
    patch_core(const string_vector &pSource, const string_vector &pPatch);

    /**
     * Perform patch
     * @return true in case of success, otherwise - false
     */
    bool patch();

    /**
     * Get patch result
     * @return lines of patched file
     */
    string_vector patch_lines();

    /**
     * Returns message of last error occurred
     */
    std::string last_error();

private:
    /**
     * Reads numbers from hunk header: @@ -a,b +c,d @@
     * @param pHunkHeader hunk line
     * @param pOutSourceStart (output param &) starting line number in original file
     * @param pOutSourceCount (output param &) amount of untouched and removed lines
     * @param pOutNewStart (output param &) starting line number in resulting file
     * @param pOutNewCount (output param &) amount of untouched and added lines
     * @return true - in case of successful parse, otherwise - false
     */
    bool parse_patch_hunk(const std::string &pHunkHeader, ulong &pOutSourceStart, ulong &pOutSourceCount,
                          ulong &pOutNewStart, ulong &pOutNewCount);

    /**
     * Checks if line represents definition of one of source files
     * @param pString
     * @return bool
     */
    bool is_file_def(const std::string &pString) const;
    std::string mError;
    const string_vector &mSource;
    const string_vector &mPatch;
    string_vector mPatched;
};


#endif //PDIFF_PATCH_CORE_H
