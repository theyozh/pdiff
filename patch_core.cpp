//
// Created by igor on 21.01.18.
//

#include "patch_core.hpp"

#include <algorithm>
#include <regex>
#include <utility>

// Helper - line specifier (number and line itself)
struct line {
    ulong n;
    std::string s;

    line(const ulong &number, std::string string): n(number), s(string) {}
};

/**
 * Helper that allows to perform search over vector of line structs
 */
struct find_line : std::unary_function<line, bool> {
    ulong mLineNumber;
    find_line(ulong pLineNumber): mLineNumber(pLineNumber) {}
    bool operator()(line const &l) const {
        return l.n == mLineNumber;
    }
};

patch_core::patch_core(const string_vector &pSource, const string_vector &pPatch) :
        mSource(pSource), mPatch(pPatch) {}

bool patch_core::patch() {
    if (mPatch.empty() || (1 == mPatch.size() && mPatch[0].empty())) {
        mPatched = mSource;
        return true;
    }
    return false; // temp
    std::vector<line> sourceLines;
    ulong i(0);
    for (const auto &s: mSource) {
        sourceLines.emplace_back(++i, s);
    }

    ulong sourceStart, sourceCount(0), newStart, newCount(0);
    ulong sourceActualCount(0), newActualCount(0);

    auto it = mPatch.begin(); // here we skip info about files used for patch generation
    auto sourceIt = sourceLines.end();
    while (mPatch.end() != it) { //probably turn it into do while
        if (is_file_def(*it)) {
            ++it;
            continue;
        }

        //tbd

        ++it;
    }

}

string_vector patch_core::patch_lines() {
    return mPatched;
}

std::string patch_core::last_error() {
    return mError;
}

bool patch_core::parse_patch_hunk(const std::string &pHunkHeader, ulong &pOutSourceStart, ulong &pOutSourceCount,
                                  ulong &pOutNewStart, ulong &pOutNewCount) {
    bool parsedSuccessful(false);
    try {
        std::regex re(R"(^@@ -(\d+),(\d+) \+(\d+),(\d+) @@.*)");
        std::smatch match;
        bool matched = std::regex_match(pHunkHeader, match, re);
        if (matched && match.size() > 1) {
            pOutSourceStart = std::stoul(match.str(1));
            pOutSourceCount = std::stoul(match.str(2));
            pOutNewStart = std::stoul(match.str(3));
            pOutNewCount = std::stoul(match.str(4));
            parsedSuccessful = true;
        } else {
            mError = "Line started with @ is not a patch hunk header";
        }
    } catch (std::regex_error &error) {
        mError = error.what();
    }
    return parsedSuccessful;
}

bool patch_core::is_file_def(const std::string &pString) const {
    if (pString.size() > 2) {
        std::string beginning = pString.substr(0, 3);
        if (beginning == "---" || beginning == "+++") {
            return true;
        }
    }
    return false;
}

