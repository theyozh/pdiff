//
// Created by igor on 19.01.18.
//

#ifndef PDIFF_UTILS_H
#define PDIFF_UTILS_H

#include <string>
#include <vector>

bool file_exists(const std::string &pFileName);

std::string file_mtime(const std::string &pFileName);

std::vector<std::string> read_file(const std::string &pFileName);


#endif //PDIFF_UTILS_H
