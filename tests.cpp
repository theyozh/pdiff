//
// Created by igor on 23.01.18.
//

#include "catch.hpp"
#include "diff_core.hpp"
#include "patch_core.hpp"

#include <iostream>

#define DISABLE_PATCH_TESTS

using Catch::Matchers::Equals;

extern std::vector<std::string> read_file(const std::string &pFileName);

SCENARIO("File reading", "[utils]") {
    string_vector lines;
    string_vector reference = {"this text is written", "here just for testing", "purposes"};

    auto check = [&]() -> void {
        REQUIRE_THAT(lines, Equals(reference));
    };

    SECTION("reading file without trailing newline") {
        lines = read_file("tests/p10/a");
        check();
    }

    SECTION("reading file with trailing newline") {
        lines = read_file("tests/p10/b");
        reference.emplace_back("and other stuff");
        reference.emplace_back("");
        check();
    }
}

SCENARIO("diff calculation", "[diff]") {
    string_vector source;
    string_vector modified;
    string_vector reference;
    diff_core *differ(nullptr);
    string_vector diff;

    auto load_test = [&](const std::string &pTestId) -> void {
        source = read_file("tests/" + pTestId + "/a");
        modified = read_file("tests/" + pTestId + "/b");
        reference = read_file("tests/" + pTestId + "/diff");

        if (reference.size() > 2) {
            // here we skip heading lines in diff file
            string_vector(reference.begin() + 2, reference.end()).swap(reference);
        }
        if (!reference.empty()) {
            auto rit = reference.end() - 1;
            reference.erase(rit); // WA caused by differences in diff_core launch ways
        }
    };

    auto do_test = [&](const std::string &pTestId) -> void {
        load_test(pTestId);
        differ = new diff_core(source, modified);
        diff = differ->diff();

        REQUIRE_THAT(diff, Equals(reference));
    };

    SECTION("diff successfully calculated") {
        WHEN("source is empty and reference is not") { do_test("p01"); }
        WHEN("source is empty and reference too") { do_test("p02"); }
        WHEN("source does not have trailing new line and reference does") { do_test("p03"); }
        WHEN("source have trailing new line and reference does not") { do_test("p04"); };
        WHEN("source should be patched from the beginning") { do_test("p05"); }
        WHEN("source should be patched in the middle") { do_test("p06"); }
        WHEN("source should be patched at the end") { do_test("p07"); }
        WHEN("patch contains several hunks") { do_test("p08"); }
        WHEN("source is not empty but reference is") { do_test("p09"); }
        WHEN("source does not have trailing newline and should be patched "
                     "at the end and reference does have trailing newline") { do_test("p10"); }
        WHEN("source does have trailing newline and should be patched "
                     "at the end and reference does not have trailing newline") { do_test("p11"); }
        WHEN("source is equal to reference and both are not empty") { do_test("p12"); }
        WHEN("both files are equal and are not empty and both do not have trailing newline") { do_test("p13"); }
        WHEN("the last line differ in files and both do not have trailing newline") { do_test("p14"); }
        WHEN("source and reference files are not empty and reference file has totally new content") { do_test("p15"); }
    }
    delete differ; // without if(differ) because delete of nullptr has no effect
}

#ifndef DISABLE_PATCH_TESTS //disabled since patch part is not fully implemented
SCENARIO("patch application", "[patch]") {
    string_vector source;
    string_vector patch;
    string_vector reference;
    bool errorOccurred(true); // true by default in order not to forget to implement all cases
    patch_core *patcher(nullptr);

    auto load_test = [&](const std::string &pTestId) -> void {
        source = read_file("tests/" + pTestId + "/a");
        patch = read_file("tests/" + pTestId + "/diff");
        reference = read_file("tests/" + pTestId + "/b");
    };

    auto do_test = [&](const std::string &pTestId) -> void {
        load_test(pTestId);
        patcher = new patch_core(source, patch);
        errorOccurred = patcher->patch();

        CHECK_FALSE(errorOccurred);
        if (errorOccurred) {
            INFO(patcher->last_error());
        }
        REQUIRE_THAT(patcher->patch_lines(), Equals(reference));
    };

    auto do_test_error = [&](const std::string &pTestId, const std::string &pExpectedError) -> void {
        load_test(pTestId);
        patcher = new patch_core(source, patch);
        errorOccurred = patcher->patch();

        CHECK(errorOccurred);
        REQUIRE_THAT(patcher->last_error(), Equals(pExpectedError));
    };

    SECTION("patch can be applied") {
        WHEN("source is empty and reference is not") { do_test("p01"); }
        WHEN("source is empty and reference too") { do_test("p02"); }
        WHEN("source does not have trailing new line and reference does") { do_test("p03"); }
        WHEN("source have trailing new line and reference does not") { do_test("p04"); };
        WHEN("source should be patched from the beginning") { do_test("p05"); }
        WHEN("source should be patched in the middle") { do_test("p06"); }
        WHEN("source should be patched at the end") { do_test("p07"); }
        WHEN("patch contains several hunks") { do_test("p08"); }
        WHEN("source is not empty but reference is") { do_test("p09"); }
        WHEN("source does not have trailing newline and should be patched "
                     "at the end and reference does have trailing newline") { do_test("p10"); }
        WHEN("source does have trailing newline and should be patched "
                     "at the end and reference does not have trailing newline") { do_test("p11"); }
        WHEN("source is equal to reference and both are not empty") { do_test("p12"); }
        WHEN("both files are equal and are not empty and both do not have trailing newline") { do_test("p13"); }
        WHEN("the last line differ in files and both do not have trailing newline") { do_test("p14"); }
        WHEN("source and reference files are not empty and reference file has totally new content") { do_test("p15"); }
    }

    SECTION ("patch canNOT be applied") {
        WHEN("some patch hunk contains wrong number of modified lines in source file") {
            do_test_error("pf01", "Patch file is not valid. One of hunks contains wrong counters.");
        }
        WHEN("some patch hunk contains wrong number of modified lines in reference file") {
            do_test_error("pf02", "Patch file is not valid. One of hunks contains wrong counters.");
        }
        WHEN("patch is not applicable to source") {
            do_test_error("pf03", "Patch file cannot be applied to source file.");
        }
    }

    delete patcher; // without if(patcher) because delete of nullptr has no effect
}

#endif // #ifndef DISABLE_PATCH_TESTS