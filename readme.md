# Build instructions

```text
git clone https://gitlab.com/theyozh/pdiff.git
cd pdiff
cmake .
make
```

# Tests running
```text
./pdiff-test -s
```

# Producing diff of two files
```text
./pdiff file1 file2
```

# Putting diff of two files into third file
```text
./pdiff file1 file2 > file3
```

# Printing help message
```text
./pdiff -h
```

Help message sample
```text
In order to show this message pass -h flag:
    pdiff -h

In order to get diff of two files pass their names as arguments. Example:
    pdiff file1 file2

In order to save diff of two files into patch file do the following:
    pdiff file1 file2 > file.patch

    , where file1, file2, and file.patch are examples of file names.

In case of identical files no output will be produced.
If two files are different then patch in unified-like format will be printed.

In order to patch some file pass its name and name of patch file preceded by -p flag:
    pdiff file1 -p file.patch

```

# Patch applying (tbd)
```text
./pdiff file1 -p file2.patch
```

